const express = require("express"),
    app = express(),
    fs = require("fs"),
    shell = require("shelljs"),
    folderPath = "./ConsumoResponses/",
    defaultFileExtension = "json",
    bodyParser = require("body-parser"),
    path = require("path"),
    SchemaObject = require("schema-object");

shell.mkdir("-p", folderPath);

app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

app.get("/", (req, res) =>
    res.send("Hola!! Estoy listo ¡Manda esas peticiones!")
);

app.post("/write", (req, res) => {
    let extension = req.body.fileExtension || defaultFileExtension,
        filePath = `${path.join(folderPath, req.body.requestName)}.${extension}`;

    let onlyResponsesFilePath = `${path.join(
        folderPath,
        req.body.requestName + "_responses"
    )}.${extension}`;

    fs.exists(filePath, function(exists) {
        if (exists) {
            console.log("Ya existe el archivo");

            fs.readFile(filePath, function readFileCallback(err, data) {
                if (err) {
                    console.log(err);
                } else {
                    var obj = [];

                    console.log("FileData:", data);

                    obj.push(data);
                    obj.push(req.body.requestData);
                    obj.push(req.body.responseData);

                    // var responses = [];
                    // responses.push(JSON.parse(data));
                    //
                    // console.log(responses);
                    // responses.push(req.body.responseData);
                    //
                    // console.log(
                    //     "Los datos de la peticion son: " + JSON.stringify(req.body)
                    // );
                    console.log("añadiendo datos...");
                    console.log(req.body);

                    fs.writeFile(filePath, obj, err => {
                        console.log("escribiendo en el archivo: " + filePath);
                        if (err) {
                            console.log(err);
                            res.send("Error");
                        } else {
                            res.send("Success");
                        }
                    });

                    // fs.writeFile(onlyResponsesFilePath, responses, err => {
                    //     console.log("escribiendo en el archivo: " + onlyResponsesFilePath);
                    //     if (err) {
                    //         console.log(err);
                    //         res.send("Error");
                    //     } else {
                    //         res.send("Success");
                    //     }
                    // });
                }
            });
        } else {
            console.log("El archivo no existe");
            console.log(req.body);

            var obj = [];

            obj.push(req.body.requestData);
            obj.push(req.body.responseData);

            fs.writeFile(filePath, obj, err => {
                console.log("Creando archivo: " + filePath);
                if (err) {
                    console.log(err);
                    res.send("Error");
                } else {
                    res.send("Success");
                }
            });

            // var responses;
            //   responses.push(req.body.responseData);

            // var Objeto = new SchemaObject({
            //     request: {},
            //     response: {}
            // });
            //
            // var objetito = new Objeto({
            //     request: req.body.requestData,
            //     response: req.body.responseData
            // });
            //
            // var responses = [];
            //
            // responses.push(objetito);
            //
            // responses = JSON.stringify(responses);
            //
            // console.log(responses, "Este es el objettito");
            //
            // fs.writeFile(onlyResponsesFilePath, responses, err => {
            //     console.log("Creando archivo: " + onlyResponsesFilePath);
            //     if (err) {
            //         console.log(err);
            //         res.send("Error");
            //     } else {
            //         res.send("Success");
            //     }
            // });
        }
    });
});

app.listen(3000, () => {
    console.log("La aplicacion se inicio correctamente ¡Lanza esas peticiones! ");
    console.log(
        `Los datos se almacenaran en: ${path.join(process.cwd(), folderPath)}`
    );
});